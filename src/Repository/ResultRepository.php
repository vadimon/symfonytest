<?php

namespace App\Repository;

use App\Entity\Result;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Result|null find($id, $lockMode = null, $lockVersion = null)
 * @method Result|null findOneBy(array $criteria, array $orderBy = null)
 * @method Result[]    findAll()
 * @method Result[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Result::class);
    }

    // /**
    //  * @return Result[] Returns an array of Result objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Result
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $idCompany
     * @return Result[]
     */
    public function findAllIdCompany($idCompany): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT result.id,result.id_company,result.id_resume,company.id,company.title_company,resume.id,resume.title_resume
            FROM result
            JOIN company ON result.id_company = company.id
            JOIN resume ON result.id_resume = resume.id
            WHERE result.id_company = $idCompany";
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idCompany' => $idCompany]);

        return $stmt->fetchAll();
    }


    /**
     * @param  $feedback
     * @return Result[]
     */
    public function statistics($feedback): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT result.id,result.id_company,result.id_resume,result.feedback,company.id,company.title_company,resume.id,resume.title_resume
            FROM result
            JOIN company ON result.id_company = company.id
            JOIN resume ON result.id_resume = resume.id
            WHERE result.feedback = $feedback";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
