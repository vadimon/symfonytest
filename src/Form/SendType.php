<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Result;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SendType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idCompany', EntityType::class, [
                'label' => 'Виберіть компанію',
                'class' => Company::class,
                'choice_label' => function(Company $company) {
                    return $company->getTitleCompany();
                },
                'choice_value' => function(Company $company = null) {
                    return $company ? $company->getId() : '';
                },
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Result::class,
        ]);
    }
}
