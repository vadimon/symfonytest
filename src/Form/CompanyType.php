<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleCompany', TextType::class, ['label' => 'Назва компанії'])
            ->add('siteCompany', TextType::class, ['label' => 'Сайт'])
            ->add('adressCompany', TextType::class, ['label' => 'Адреса'])
            ->add('phoneCompany', TextType::class, ['label' => 'Номер тел.'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
