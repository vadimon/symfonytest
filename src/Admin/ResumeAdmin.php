<?php


namespace App\Admin;

use App\Entity\Resume;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelHiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ResumeAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Resume
            ? $object->getTitleResume()
            : 'Resume';
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('titleResume', TextType::class)
            ->add('description', TextareaType::class)
            ->add('dateCreate', DateType::class)
            ->add('dateUpdate', DateType::class)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('titleResume')
            ->add('dateCreate')
            ->add('dateUpdate')
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('titleResume');
    }

}