<?php


namespace App\Admin;

use App\Entity\Resume;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ResultAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Resume
            ? $object->getTitleResume()
            : 'Resume';
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('dateSent', DateTimeType::class)
            ->add('feedback', TextType::class)
            ->add('adressCompany', TextType::class)
            ->add('phoneCompany', TextType::class)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('name');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('name');
    }

}