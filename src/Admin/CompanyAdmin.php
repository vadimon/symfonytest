<?php


namespace App\Admin;

use App\Entity\Company;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CompanyAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Company
            ? $object->getTitleCompany()
            : 'Company';
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('titleCompany', TextType::class)
            ->add('siteCompany', TextType::class)
            ->add('adressCompany', TextType::class)
            ->add('phoneCompany', TextType::class)
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('titleCompany')
            ->add('siteCompany')
            ->add('adressCompany')
            ->add('phoneCompany')
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('titleCompany');
    }

}