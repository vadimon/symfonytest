<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title_company", type="string", length=255, nullable=false)
     */
    private $titleCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="site_company", type="string", length=255, nullable=false)
     */
    private $siteCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="adress_company", type="string", length=255, nullable=false)
     */
    private $adressCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_company", type="string", length=255, nullable=false)
     */
    private $phoneCompany;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleCompany(): ?string
    {
        return $this->titleCompany;
    }

    public function setTitleCompany(string $titleCompany): self
    {
        $this->titleCompany = $titleCompany;

        return $this;
    }

    public function getSiteCompany(): ?string
    {
        return $this->siteCompany;
    }

    public function setSiteCompany(string $siteCompany): self
    {
        $this->siteCompany = $siteCompany;

        return $this;
    }

    public function getAdressCompany(): ?string
    {
        return $this->adressCompany;
    }

    public function setAdressCompany(string $adressCompany): self
    {
        $this->adressCompany = $adressCompany;

        return $this;
    }

    public function getPhoneCompany(): ?string
    {
        return $this->phoneCompany;
    }

    public function setPhoneCompany(string $phoneCompany): self
    {
        $this->phoneCompany = $phoneCompany;

        return $this;
    }
    public function __toString(){
        return $this->titleCompany;
    }


}
