<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Result
 *
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 * @ORM\Table(name="result", indexes={@ORM\Index(name="id_resume", columns={"id_resume"}), @ORM\Index(name="id_company", columns={"id_company"})})
 *
 */
class Result
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sent", type="datetime", nullable=false)
     */
    private $dateSent;

    /**
     * @var int
     *
     * @ORM\Column(name="feedback", type="string", nullable=true)
     */
    private $feedback;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_company", referencedColumnName="id")
     * })
     */
    private $idCompany;

    /**
     * @var \Resume
     *
     * @ORM\ManyToOne(targetEntity="Resume")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_resume", referencedColumnName="id")
     * })
     */
    private $idResume;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateSent(): ?\DateTimeInterface
    {
        return $this->dateSent;
    }

    public function setDateSent(\DateTimeInterface $dateSent): self
    {
        $this->dateSent = $dateSent;

        return $this;
    }

    public function getFeedback(): ?string
    {
        return $this->feedback;
    }

    public function setFeedback(string $feedback): self
    {
        $this->feedback = $feedback;

        return $this;
    }

    public function getIdCompany(): ?Company
    {
        return $this->idCompany;
    }

    public function setIdCompany(?Company $idCompany): self
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    public function getIdResume(): ?Resume
    {
        return $this->idResume;
    }

    public function setIdResume(?Resume $idResume): self
    {
        $this->idResume = $idResume;

        return $this;
    }



}
