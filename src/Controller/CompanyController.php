<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Result;
use App\Form\CompanyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    /**
     * @Route("/company", name="company")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository(Company::class)->findAll();

        return $this->render('company/index.html.twig', [
            'company' => $company
        ]);
    }

    /**
     * @Route("/company/single/{company}", name="single_company")
     */
    public function single(Company $company)
    {
        return $this->render('company/singl.html.twig',[
           'company' => $company,
        ]);
    }


    /**
     * @Route("/company/create", name="create_company")
     */
    public function create(Request $reguest)
    {
        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);

        $form->handleRequest($reguest);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('company');
        }

        return $this->render('company/form.html.twig',[
            'form' => $form->createView()
            ]);
    }

    /**
     * @Route("/company/update/{company}", name="update_company")
     */
    public function update(Request $request, Company $company)
    {
        $form = $this->createForm(CompanyType::class, $company, [
            'action' => $this->generateUrl('update_company', [
                'company' => $company->getId()
            ]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('company');
        }
        return $this->render('company/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/company/delete/{company}", name="delete_company")
     */
    public function delete(Company $company)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($company);
        $em->flush();

        return $this->redirectToRoute('company');
    }

    /**
     * @Route("/company/show/{company}", name="show_resume")
     */
    public function show(Company $company)
    {
        $idCompany = $company->getId();
        $show = $this->getDoctrine()
            ->getRepository(Result::class)
            ->findAllIdCompany($idCompany);

        return $this->render('company/show.html.twig', [
            'show' => $show
        ]);
    }
}
