<?php

namespace App\Controller;

use App\Entity\Result;
use App\Entity\Resume;
use App\Form\ResultType;
use App\Form\SendType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ResultController extends AbstractController
{
    /**
     * @Route("/result", name="result")
     */
    public function index()
    {
        return $this->render('result/index.html.twig');
    }

    /**
     * @Route("/result/statistic/{feedback}", name="statistic_result")
     */
    public function statistic($feedback)
    {
        $result = $this->getDoctrine()
            ->getRepository(Result::class)
            ->statistics($feedback);

        return $this->render('result/statistic.html.twig', [
            'result' => $result
        ]);
    }

    /**
     * @Route("/result/send/{resume}", name="send_result")
     */
    public function send(Request $request, Resume $resume)
    {
        $result = new Result();
        $form = $this->createForm(SendType::class, $result);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $form->getData();

            $result->setDateSent(new \DateTime('now'));
            $result->setIdResume($resume);

            $em = $this->getDoctrine()->getManager();
            $em->persist($result);
            $em->flush();

            return $this->redirectToRoute('resume');
        }
        return $this->render('result/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/result/update/{result}", name="update_result")
     */
    public function update(Request $request, Result $result)
    {
        $form = $this->createForm(ResultType::class, $result,  [
            'action' => $this->generateUrl('update_result', [
                'result' => $result->getId()
            ]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('company');
        }
        return $this->render('result/form_update.html.twig', [
           'form' => $form->createView()
        ]);
    }

}
