<?php

namespace App\Controller;

use App\Entity\Resume;
use App\Form\ResumeType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ResumeController extends AbstractController
{
    /**
     * @Route("/resume", name="resume")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $resume = $em->getRepository(Resume::class)->findAll();

        return $this->render('resume/index.html.twig', [
            'controller_name' => 'ResumeController',
            'resume' => $resume
        ]);
    }

    /**
     * @Route("/resume/single/{resume}", name="single_resume")
     */
    public function single(Resume $resume)
    {
        return $this->render('resume/single.html.twig',[
            'resume' => $resume,
        ]);
    }


    /**
     * @Route("/resume/create", name="create_resume")
     */
    public function create(Request $reguest, FileUploader $fileUploader)
    {
        $resume = new Resume();
        $form = $this->createForm(ResumeType::class, $resume);

        $form->handleRequest($reguest);

        if ($form->isSubmitted() && $form->isValid()) {
            $resume = $form->getData();
            $resume->setDateCreate(new \DateTime('now'));

            $file = $form['file']->getData();
            $fileName = $fileUploader->upload($file);
            $resume->setFile($fileName);


            $em = $this->getDoctrine()->getManager();

            $em->persist($resume);
            $em->flush();

            return $this->redirectToRoute('resume');
        }

        return $this->render('resume/form.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }


    /**
     * @Route("/resume/update/{resume}", name="update_resume")
     */
    public function update(Request $request, Resume $resume)
    {
        $form = $this->createForm(ResumeType::class, $resume, [
            'action' => $this->generateUrl('update_resume', [
                'resume' => $resume->getId()
            ]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resume = $form->getData();
            $resume->setDateUpdate(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('resume');
        }
        return $this->render('resume/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/resume/delete/{resume}", name="delete_resume")
     */
    public function delete(Resume $resume)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($resume);
        $em->flush();

        return $this->redirectToRoute('resume');
    }
}
